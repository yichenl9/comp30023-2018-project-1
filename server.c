/* A simple server with multi-thread in the internet domain using TCP
The port number is passed as an argument 
The root directory is passed as second argument

name: Yichen Lin
login ID: yichenl9

*/

/*
include files
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>


#define BACKLOG 5
#define MAXSIZE 1024

void pthread_receive(void *arg);
void respondHandle(int newfd, char *rootdir);
void sendheader(int newfd, int status);
void sendMime(int newfd, char *ext);
void sendData(int newfd, char *rootdir, char *file);
char *concat(char *s1, char *s2);

typedef struct args{
    char *rootdir;
    int connfd ;
} ARG;

/* reference file: https://github.com/iFuSiiOnzZ/http/blob/master/server.c */
int main(int argc, char *argv[]){


    int socketDescriptorClient  = 0;
    int socketDescriptor        = 0;
    int socketLength            = 0;
    int socketPort;
    pthread_t thread            = 0;
    
    struct sockaddr_in  s_server = { 0 };
    struct sockaddr_in  s_client = { 0 };
    
    
    ARG *arguments = malloc(sizeof(ARG));

    /* check arguments */
    if (argc < 2) {
        printf("ERROR no port provided");
        exit(1);
    }
    if (argc < 3) {
        printf("ERROR no path provided");
        exit(1);
    }

    /* Create TCP socket */
    if((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("ERROR opening socket");
        exit(1);
    }
    socketPort = atoi(argv[1]);
    
    /* Create address we're going to listen on (given port number)
	 - converted to network byte order & any IP address for 
	 this machine */
    s_server.sin_family         = AF_INET;
    s_server.sin_addr.s_addr    = INADDR_ANY;
    s_server.sin_port           = htons(socketPort);
    
    /* Bind address to the socket */
    if(bind(socketDescriptor, (struct sockaddr *) &s_server, (socklen_t) sizeof(struct sockaddr_in)) == -1)
    {
        perror("ERROR on binding");
		exit(1);
    }
    
    /* Listen on socket - means we're ready to accept connections - 
	 incoming connection requests will be queued */
    if(listen(socketDescriptor, 5) == -1)
    {
        perror("ERROR on listening");
        exit(1);
    }

    /* Accept a connection - create thread to accept multi requests from cilents concurrently. */
    while(1)
    {
        
        socketLength = sizeof(struct sockaddr_in);
        if((socketDescriptorClient = accept(socketDescriptor, (struct sockaddr *) &s_client, &socketLength)) != -1)
        {

            /*pass the client socket and the root path as arguments in pthread*/
            arguments->connfd = (long)socketDescriptorClient;
            arguments->rootdir = argv[2];
            if(pthread_create(&thread, NULL, (void *) &pthread_receive, (void *)arguments) != 0)
            {
                perror("ERROR on creating thread");
            }
        }
    }

    close(socketDescriptor);
    return 0;
}

/* Handle the thread by passing the client socket and root path to the
    client request handle function
    then close the client socket and exit the thread
*/
void pthread_receive(void* arg){
    
    ARG *info = (ARG *)arg;
    
    respondHandle(info->connfd, info->rootdir);
    
    close(info->connfd);
    pthread_exit(NULL);
}

/* Handle the request from client by Reading characters from the connection,
	then process sending data from server data to client*/ 
void respondHandle(int newfd, char *rootdir) {

    char buff[MAXSIZE];

    bzero(buff, MAXSIZE);
    read(newfd, buff, sizeof(buff) - 1);


    printf("%s", buff);

    char *method = strtok(buff, " /");
    char *filename = strtok(NULL, " \t");

    sendData(newfd, rootdir, filename);

}

/* Sending data from server by reading the request file path from client request
    and open file from local,
    then write to the client */
/* reference : http://www.junlz.com/?p=1014 */

void sendData(int newfd, char *rootdir, char *file) {
    char buff[MAXSIZE];
    int bytesread;
    FILE *fp;
    char *temp;
    char *dirfile = concat(rootdir, file);

    /* open file in bytes */
    fp = fopen(dirfile, "rb");

    /* if file can't be open, send 404 error and close the current request */
    if (fp == NULL) {
        sendheader(newfd, 2);
        close(newfd);  
    }

    /* Otherwise send 200 and get file extension, then send mime type, 
    and read file, then write to client in bytes */
    else {
        sendheader(newfd, 1);

        char *ext = strrchr(file, '.');
        sendMime(newfd, ext);

        while (!feof(fp)) {
            bytesread = fread(buff, 1, sizeof(buff) - 1, fp);
            write(newfd, buff, bytesread);
        }

        fclose(fp);
        close(newfd);
    }
}

/* Send the currect header */
void sendheader(int newfd, int status){

    char *header;
    char *version = "HTTP/1.0";

    if(status == 1){
        header = concat(version, " 200 OK\r\n");
    }else{
        header = concat(version, " 404 NOT FOUND\r\n");
    }

    write(newfd, header, strlen(header));
}


/* Send the currect MIME type */
void sendMime(int newfd, char *ext){
    char *mime;
    char *content = "Content-Type: ";

    if (strncmp(ext, ".css", sizeof(".css")) == 0) {
        mime = concat(content, "text/css\r\n\r\n");
    }
    else if (strncmp(ext, ".html", sizeof(".html")) == 0) {
        mime = concat(content, "text/html\r\n\r\n");
    }
    else if (strncmp(ext, ".jpg", sizeof(".jpg")) == 0) {
        mime = concat(content, "image/jpeg\r\n\r\n");
    }
    else if (strncmp(ext, ".js", sizeof(".js")) == 0) {
        mime = concat(content, "text/javascript\r\n\r\n");
    }
    write(newfd, mime, strlen(mime));

}


/* Helper function to combine 2 strings */
/* reference on concatenate 2 strings : https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c */
char *concat(char *s1, char *s2) {
    size_t len1 = strlen(s1);
    size_t len2 = strlen(s2);
    char* string = malloc (len1 + len2 + 1);
    memcpy(string, s1, len1);
    memcpy(string + len1, s2, len2 + 1);
    return string;
}